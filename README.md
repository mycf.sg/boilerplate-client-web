# mcf-client-web

### Build

    yarn install
    yarn clean                      # removes /dist
    yarn build                      # test build, builds in /dist
    yarn build:prod                 # production build, builds in /dist

### Test

    yarn test                       # runs unit tests once
    yarn test:watch                 # runs unit tests using karma in watch mode
    yarn tsc:check                  # runs typescript typechecking
    yarn tsc:check-config           # runs typescript typechecking on .js config files
    yarn lint                       # runs tslint, stylelint, tsc:check
    yarn lint:fix
    yarn coverage

### Develop

    yarn local                      # runs webpack-serve
    yarn test:watch                 # runs unit tests using karma in watch mode
