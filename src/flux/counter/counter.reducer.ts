import {Reducer} from 'redux';
import {ActionType} from 'typesafe-actions';
import * as actions from './counter.actions';
import {CounterActionTypes} from './counter.constants';

export interface ICountersState {
  readonly value: number;
}

type CounterReducer = Reducer<ICountersState, ActionType<typeof actions>>;

export const counterReducer: CounterReducer = (state = {value: 0}, action) => {
  switch (action.type) {
    case CounterActionTypes.INCREMENT:
      return {
        ...state,
        value: state.value + action.payload,
      };
    case CounterActionTypes.DECREMENT:
      return {
        ...state,
        value: state.value - parseInt(action.payload, 10),
      };
    default:
      return state;
  }
};
