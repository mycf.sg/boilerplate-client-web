import {decrement, increment} from '../counter.actions';
import {counterReducer} from '../counter.reducer';

describe('counter reducer', () => {
  describe('INCREMENT', () => {
    test('should increment value by 1 when the payload is not set', () => {
      expect(counterReducer({value: 0}, increment(1))).toEqual({
        value: 1,
      });
    });
    test('should increment value by 3 when the payload is set to 3', () => {
      expect(counterReducer({value: 0}, increment(3))).toEqual({
        value: 3,
      });
    });
  });
  describe('DECREMENT', () => {
    test('should decrement value by 1 when the payload is not set', () => {
      expect(counterReducer({value: 0}, decrement('1'))).toEqual({
        value: -1,
      });
    });
    test('should decrement value by 3 when the payload is set to 3', () => {
      expect(counterReducer({value: 0}, decrement('3'))).toEqual({
        value: -3,
      });
    });
  });
});
