import {delay} from 'redux-saga';
import {put, takeLatest} from 'redux-saga/effects';
import {increment} from '~/flux/counter/counter.actions';
import {CounterActionTypes} from '~/flux/counter/counter.constants';

function* asyncIncrement() {
  yield delay(1000);
  yield put(increment());
}

export function* counterSaga() {
  yield [takeLatest(CounterActionTypes.ASYNC_INCREMENT, asyncIncrement)];
}
