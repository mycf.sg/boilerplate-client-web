import {action} from 'typesafe-actions';
import {CounterActionTypes} from './counter.constants';

export const asyncIncrement = (value: number = 1) => action(CounterActionTypes.ASYNC_INCREMENT, value);
export const increment = (value: number = 1) => action(CounterActionTypes.INCREMENT, value);

// change the type of the expected parameter to make sure the type is well handled in the reducer
export const decrement = (value: string = '1') => action(CounterActionTypes.DECREMENT, value);
