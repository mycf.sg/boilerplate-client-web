import {spawn} from 'redux-saga/effects';
import {counterSaga} from './counter/counter.sagas';

export function* rootSaga() {
  // use spawn to have each sagas independant
  yield [spawn(counterSaga)];
}
