import {LocationChangeAction, RouterAction, routerMiddleware, routerReducer, RouterState} from 'react-router-redux';
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {StateType} from 'typesafe-actions';
import {counterReducer, ICountersState} from '~/flux/counter';
import {rootSaga} from '~/flux/index.sagas';
import {history} from '~/router';
import * as counterActions from './counter/counter.actions';

export interface IAppState {
  readonly counter: ICountersState;
  readonly router: RouterState;
}

type ReactRouterAction = RouterAction | LocationChangeAction;
export type IAppAction = ReactRouterAction | StateType<typeof counterActions>;

const appReducer = combineReducers<IAppState>({
  counter: counterReducer,
  router: routerReducer,
});

const sagaMiddleware = createSagaMiddleware();
const middleware = [routerMiddleware(history), sagaMiddleware];

function configureStore(initialState?: object) {
  // TypeScript definitions for devtools in /my-globals/index.d.ts
  // Redux devtools are still enabled in production!
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        actionsBlacklist: [],
      })
    : compose;
  // create store
  return createStore(appReducer, initialState!, composeEnhancers(applyMiddleware(...middleware)));
}

// pass an optional param to rehydrate state on app start
export const store = configureStore();
sagaMiddleware.run(rootSaga);
