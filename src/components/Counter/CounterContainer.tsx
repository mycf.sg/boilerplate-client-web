import {connect} from 'react-redux';
import {IAppState} from '~/flux';
import {asyncIncrement, decrement, increment} from '~/flux/counter/counter.actions';
import {Counter} from './Counter';

export const CounterContainer = connect(
  (state: IAppState) => ({counter: state.counter.value}),
  {increment, decrement, asyncIncrement},
)(Counter);
