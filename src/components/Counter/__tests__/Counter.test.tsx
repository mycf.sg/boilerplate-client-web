import {BaseButton} from '@govtechsg/mcf-mcfui';
import {shallow} from 'enzyme';
import React from 'react';
import {Counter} from '../Counter';

// bear with me, I used shallow only because of https://github.com/airbnb/enzyme/issues/1566
describe('Counter', () => {
  const decrement = jest.fn();
  const increment = jest.fn();
  const asyncIncrement = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
  });
  test('should call decrement function when clicking on decrement button', () => {
    const component = shallow(
      <Counter counter={6} asyncIncrement={asyncIncrement} increment={increment} decrement={decrement} />,
    );
    const decrementButton = component.find(BaseButton).findWhere((button) => button.text() === 'Decrement');
    decrementButton.parent().simulate('click');
    expect(asyncIncrement).not.toHaveBeenCalledTimes(1);
    expect(increment).not.toHaveBeenCalledTimes(1);
    expect(decrement).toHaveBeenCalledTimes(1);
  });
  test('should call increment function when clicking on increment button', () => {
    const component = shallow(
      <Counter counter={6} asyncIncrement={asyncIncrement} increment={increment} decrement={decrement} />,
    );
    const decrementButton = component.find(BaseButton).findWhere((button) => button.text() === 'Increment');
    decrementButton.parent().simulate('click');
    expect(asyncIncrement).not.toHaveBeenCalledTimes(1);
    expect(increment).toHaveBeenCalledTimes(1);
    expect(decrement).not.toHaveBeenCalledTimes(1);
  });
  test('should call incrementAsync function when clicking on incrementAsync button', () => {
    const component = shallow(
      <Counter counter={6} asyncIncrement={asyncIncrement} increment={increment} decrement={decrement} />,
    );
    const decrementButton = component.find(BaseButton).findWhere((button) => button.text() === 'Async Increment');
    decrementButton.parent().simulate('click');
    expect(asyncIncrement).toHaveBeenCalledTimes(1);
    expect(increment).not.toHaveBeenCalledTimes(1);
    expect(decrement).not.toHaveBeenCalledTimes(1);
  });
});
