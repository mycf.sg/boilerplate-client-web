import {BaseButton, TYPE} from '@govtechsg/mcf-mcfui';
import React from 'react';
import {config} from '~/config';
const styles = require('./Counter.scss');

export interface IAppProps {
  text?: string; // just to show how to handle optional props
  counter: number;
  asyncIncrement: () => any;
  increment: () => any;
  decrement: () => any;
}

export class Counter extends React.Component<IAppProps> {
  public static defaultProps = {
    text: 'unknown',
  };

  public render() {
    return (
      <div className={styles.container}>
        <h1>
          Hi from {this.props.text} - pulling data from {config.url.api}
        </h1>
        <div className={`b ${styles.counterContainer}`}>
          <span className={styles.counterValue}>{this.props.counter}</span>
          <BaseButton type={TYPE.ACTION} onClick={() => this.props.decrement()}>
            Decrement
          </BaseButton>
          <BaseButton type={TYPE.ACTION} onClick={() => this.props.increment()}>
            Increment
          </BaseButton>
          <BaseButton type={TYPE.ACTION} onClick={() => this.props.asyncIncrement()}>
            Async Increment
          </BaseButton>
        </div>
      </div>
    );
  }
}
