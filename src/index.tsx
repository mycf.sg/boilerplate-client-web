import React, {StrictMode} from 'react';
import ReactDOM from 'react-dom';
import {hot} from 'react-hot-loader';
import {Provider} from 'react-redux';
import {Route} from 'react-router-dom';
import {ConnectedRouter} from 'react-router-redux';
import 'tachyons/css/tachyons.min.css';
import './styles/main.scss';

import {store} from '~/flux';
import {history, router} from './router';

const App: React.StatelessComponent<{}> = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <StrictMode>{router.map((route, id) => <Route key={id} {...route} />)}</StrictMode>
    </ConnectedRouter>
  </Provider>
);

const render = (Component: React.StatelessComponent) =>
  process.env.NODE_ENV === 'development' ? hot(module)(Component) : Component;

const HotApp = render(App);
ReactDOM.render(<HotApp />, document.getElementById('root'));
