import createHistory from 'history/createBrowserHistory';
import {RouteProps} from 'react-router';
import {CounterContainer} from '~/components/Counter';

export const router: RouteProps[] = [{path: '/', exact: true, component: CounterContainer}];

export const history = createHistory();
