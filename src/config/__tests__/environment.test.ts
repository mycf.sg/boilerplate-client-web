const {ENVIRONMENT, getEnvironment} = require('../environment');

describe('environment configuration', () => {
  test('should return development when no value is provided', () => {
    expect(getEnvironment()).toBe(ENVIRONMENT.development);
  });

  test('should return development when empty string is provided', () => {
    expect(getEnvironment('')).toBe(ENVIRONMENT.development);
  });

  test('should return development when development is provided', () => {
    expect('development').toBe(ENVIRONMENT.development);
  });

  test('should return production when production is provided', () => {
    expect('production').toBe(ENVIRONMENT.production);
  });
});
