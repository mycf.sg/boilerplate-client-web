// @ts-check

// Only add specific environment variables (like API_URL)
// For our own safety, please don't add logic here, just use plain objects (also no functions!)

/**
 * @typedef {import('./index.d').IAppConfiguration} IAppConfiguration
 * @type {import('./index.d').IConfigurationSection<IAppConfiguration>}
 */
const configValues = {
  development: {
    url: {
      api: 'http://path.to.api.development',
    },
  },
  production: {
    url: {
      api: 'http://path.to.api.production',
    },
  },
  qa: {
    url: {
      api: 'http://path.to.api.qa',
    },
  },
  uat: {
    url: {
      api: 'http://path.to.api.uat',
    },
  },
};

module.exports = {
  configValues
}
