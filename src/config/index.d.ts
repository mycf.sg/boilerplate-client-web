export type Environment = 'development' | 'uat' | 'qa' | 'production';
export type EnvironmentEnum = {[k in Environment]: k};
export type getEnvironment = (s: string) => Environment;

export type IUrlConfiguration = {
  api: string;
};

export type IAppConfiguration = {
  url: IUrlConfiguration;
};

// This generic type is here so we can extend IAppConfiguration in ExportedConfiguration
// and still provide a definition to check against in configuration.js
export type IConfigurationSection<T> = {[k in Environment]: T};

// This is the actual exported configuration
export interface Configuration extends IAppConfiguration {
  environment: Environment;
}
