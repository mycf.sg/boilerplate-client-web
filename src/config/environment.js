// @ts-check

// Please keep the typedefs updated.

/**
 * @type {import('./index.d').EnvironmentEnum}
 */
const ENVIRONMENT = {
  development: 'development',
  uat: 'uat',
  qa: 'qa',
  production: 'production',
};

/**
 * @type {import('./index.d').getEnvironment}
 */
const getEnvironment = (env = '') => {
  return ENVIRONMENT[env] || ENVIRONMENT.development;
};

module.exports = {
  ENVIRONMENT,
  getEnvironment,
};
