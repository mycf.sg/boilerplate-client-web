// Inject the application configuration at buildtime so we do not reference
// the full configuration object at runtime and leak configuration details
// for other environments

import {Configuration} from './index.d';

declare var __WEBPACK_DEFINE_CONFIG_JS_OBJ__: Configuration;

export const config = __WEBPACK_DEFINE_CONFIG_JS_OBJ__;
