// there is this bug in development: https://github.com/postcss/postcss-custom-properties/issues/109
// which is fix in production thanks to cssnano, so ignoring for now
module.exports = {
  plugins: [
    require('postcss-preset-env')({
      stage: 0, // for nesting-rules
    }),
    require('postcss-inline-svg'),
    process.env.NODE_ENV === 'production' ? require('cssnano') : null,
  ].filter(Boolean), // remove null values
};
