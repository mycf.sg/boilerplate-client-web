const path = require('path');
const webpack = require("webpack");
const {config} = require('./src/config/build');

const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const HistoryApiFallback = require("./webpack-serve/HistoryApiFallback");

const DEV = process.env.NODE_ENV === 'development';
const PROD = process.env.NODE_ENV === 'production';


const babelEnvPreset = [
  '@babel/env',
  [
    "@babel/preset-stage-2",
    {
      "decoratorsLegacy": true
    }
  ],
  '@babel/react',
  '@babel/typescript'
];

module.exports = {
  // Defaults to development, pass --mode production to override
  mode: 'development',

  context: path.resolve(__dirname),

  target: 'web',

  entry: {
    app: ['@babel/polyfill', './src/index.tsx'],
  },

  output: {
    filename: '[name].[hash:7].js',
    path: path.resolve(__dirname, 'dist'),
    // ./ is used when hosting in a subdirectory (eg. GitHub pages)
    publicPath: PROD ? './' : '/'
  },

  module: {
    rules: [
      // Vanilla CSS
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader'],
        include: /node_modules/,
      },
      {
        test: /\.s?css$/,
        loaders: ['style-loader',
          {
            loader: 'css-loader',
            options: {
              camelCase: true,
              importLoader: true,
              localIdentName: '[name]__[local]___[hash:base64:5]',
              modules: true,
            },
          }, 'postcss-loader'],
        include: path.resolve(__dirname, "src"),
      },
      {
        test: /\.(jpg|png|gif|mp4|webm|mp3|ogg|svg)$/,
        loader: 'file-loader',
        options: {
          name: './f/[hash:16].[ext]'
        }
      },
      {
        test: /\.tsx?$/,
        exclude: /\/node_modules\//,
        loader: 'babel-loader',
        options: {
          plugins: [
            ...(DEV ? ['react-hot-loader/babel'] : [])
          ],
          presets: babelEnvPreset,
        }
      }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      __WEBPACK_DEFINE_CONFIG_JS_OBJ__: JSON.stringify(config),
    }),
    new webpack.NamedModulesPlugin(),
    // add || true and safe option to make sure webpack doesn't crash on compilation error
    new WebpackShellPlugin({
      onBuildEnd: ['yarn --silent tsc:check --pretty || true'],
      dev: false,
      safe: true,
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      favicon: './src/static/favicon.ico'
    }),
    // Generate .gz for production builds
    // Consider adding brotli-webpack-plugin if your server supports .br
    ...(PROD ? [
      new CompressionPlugin({test: /\.(js|css|html|svg)$/}),
      new BrotliPlugin({test: /\.(js|css|html|svg)$/})
    ] : [])
  ],

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /\/node_modules\//,
          filename: 'vendor.[hash:7].js',
          name: 'vendor',
          chunks: 'all'
        }
      }
    }
  },

  // Using cheap-eval-source-map for build times
  // switch to inline-source-map if detailed debugging needed
  devtool: PROD ? false : 'cheap-eval-source-map',

  serve: {
    add: app => {
      // historyApiFallback: required for redux-router to work on page refresh
      // a refresh on localhost:8080/counter will go to the counter component defined in the route
      app.use(HistoryApiFallback());
    },
    clipboard: false,
    port: 3100,
    hotClient: {
      port: 3101
    },
    devMiddleware: {
      stats: "minimal"
    }
  },

  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  },

  resolve: {
    extensions: ['.js', '.ts', '.tsx'], // TODO cant remove js it s failing
    modules: ['node_modules', path.resolve(__dirname, 'src')],
    alias: {
      '~': path.resolve(__dirname, 'src'),
    }
  }
};
